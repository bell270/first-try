﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bat::Awake()
extern void Bat_Awake_m8309A558A2641BE6BBCA1A7D4E2D9762DDD289E5 (void);
// 0x00000002 System.Void Bat::Update()
extern void Bat_Update_m159E3FCB003D507AE5439B8810B6FD8B54A0CCE8 (void);
// 0x00000003 System.Void Bat::OnMouseDown()
extern void Bat_OnMouseDown_mC220B0104C88C58379B473CB231FADB0B39695B7 (void);
// 0x00000004 System.Void Bat::OnMouseUp()
extern void Bat_OnMouseUp_mA7309C7E382EE6660EE98FC2D3F860FA7FABAFFC (void);
// 0x00000005 System.Void Bat::OnMouseDrag()
extern void Bat_OnMouseDrag_m73ACF0461F68E0B1C067953733946DB637E55260 (void);
// 0x00000006 System.Void Bat::.ctor()
extern void Bat__ctor_mC46007ECCA27DC13413CC0599448C981BE56FF29 (void);
// 0x00000007 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000008 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000009 System.Void LevelController::OnEnable()
extern void LevelController_OnEnable_m26E22F3C1AF24E609EBDE7DA2C673C633DB61859 (void);
// 0x0000000A System.Void LevelController::Update()
extern void LevelController_Update_m47511248BD50A883CE21620EC0EF69920F7D2A32 (void);
// 0x0000000B System.Void LevelController::.ctor()
extern void LevelController__ctor_m6F327591D3569C9F146532A9055BB9A1BE07D21A (void);
// 0x0000000C System.Void LevelController::.cctor()
extern void LevelController__cctor_m085176DE6AF879821EA7A4619C01A4C129255C6E (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	Bat_Awake_m8309A558A2641BE6BBCA1A7D4E2D9762DDD289E5,
	Bat_Update_m159E3FCB003D507AE5439B8810B6FD8B54A0CCE8,
	Bat_OnMouseDown_mC220B0104C88C58379B473CB231FADB0B39695B7,
	Bat_OnMouseUp_mA7309C7E382EE6660EE98FC2D3F860FA7FABAFFC,
	Bat_OnMouseDrag_m73ACF0461F68E0B1C067953733946DB637E55260,
	Bat__ctor_mC46007ECCA27DC13413CC0599448C981BE56FF29,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	LevelController_OnEnable_m26E22F3C1AF24E609EBDE7DA2C673C633DB61859,
	LevelController_Update_m47511248BD50A883CE21620EC0EF69920F7D2A32,
	LevelController__ctor_m6F327591D3569C9F146532A9055BB9A1BE07D21A,
	LevelController__cctor_m085176DE6AF879821EA7A4619C01A4C129255C6E,
};
static const int32_t s_InvokerIndices[12] = 
{
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1292,
	1530,
	1530,
	1530,
	1530,
	2554,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
