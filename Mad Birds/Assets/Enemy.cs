
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject _cloudParticlePrefab;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Bat bat = collision.collider.GetComponent<Bat>();
        if (bat != null)
        {
            Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
            return;
        }

        Enemy enemy = collision.collider.GetComponent<Enemy>();
        if (enemy != null)
        {
            return;
        }

        for (int i=0;i<collision.contactCount;i++)
        {
            if(collision.contacts[i].normal.y < -0.5)
            {
                Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
